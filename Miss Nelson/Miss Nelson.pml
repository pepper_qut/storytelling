<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Miss Nelson" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="BlackScreen" src="html/BlackScreen.png" />
        <File name="index" src="html/index.html" />
        <File name="manifest" src="html/manifest.json" />
        <File name="shortcut" src="html/shortcut.png" />
        <File name="home" src="html/home.html" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
